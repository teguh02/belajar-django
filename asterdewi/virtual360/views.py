from django.http import HttpRequest, HttpResponse
from django.shortcuts import redirect, render
from virtual360.forms import myForm

tempat = {
    "agrowisata": {
        'nama': 'Agrowisata',
        'detail_id' : "Agrowisata adalah aktivitas wisata yang melibatkan penggunaan lahan pertanian atau fasilitas terkait (misal silo dan kandang) yang menjadi daya tarik bagi wisatawan",
        'detail_en': 'Agrotourism is a tourism activity that involves the use of agricultural land or related facilities (eg silos and cages) that are attractive to tourists',
        'detail_jawa': "Agrowisata minangka kegiatan pariwisata sing nyakup panggunaan lahan pertanian utawa fasilitas sing ana gandhengane (umpamane silo lan kandang) sing narik kawigaten para wisatawan."
    },
    "taman_reptil": {
        "nama": "Taman Reptil",
        'detail_id': 'Taman reptil pada Desa Wisata Adiluhur ini memiliki konsep yang sama dengan taman-taman lainnya, yaitu taman reptil yang menyediakan berbagai macam jenis reptil, seperti kucing, anjing, burung, dan lain-lain.',
        'detail_en': 'The reptile park at Adiluhur Tourism Village has the same concept as other parks, namely a reptile park that provides various types of reptiles, such as cats, dogs, birds, and others.',
        'detail_jawa': "Taman reptil ing Desa Wisata Adiluhur nduweni konsep sing padha karo taman liyane, yaiku taman reptil sing nyedhiyakake macem-macem jinis reptil, kayata kucing, asu, manuk, lan liya-liyane."
    }
}

# Create your views here.
def index(request):
    return render(request, 'virtual360/index.html');

def detail(request):
    if request.method == 'GET':
        return redirect('/')
    elif request.method == 'POST':
        
        myform = myForm(request.POST)
        detail_tempat = myform.data['detail_tempat']
        
        return  render(request, 'virtual360/detail.html', {
            'detail': tempat[detail_tempat]
        })
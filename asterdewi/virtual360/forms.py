from django import forms

FILTER_CHOICES = (
        ('agrowisata', 'Agrowisata'),
        ('taman_reptil', 'Taman Reptil'),
    )

class myForm(forms.Form):
    detail_tempat = forms.ChoiceField(choices = FILTER_CHOICES)
from django.apps import AppConfig


class TerjemahanjawaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'terjemahanjawa'

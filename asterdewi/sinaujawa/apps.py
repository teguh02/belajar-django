from django.apps import AppConfig


class SinaujawaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sinaujawa'

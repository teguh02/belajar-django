from django.http import HttpResponse

class OnlyDesktopCanAccess:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        
        if request.user_agent.is_pc:
            response = self.get_response(request)
            return response
        else:
            return HttpResponse("<h1>Sorry, only desktop devices are allowed to access this site.</h1>")
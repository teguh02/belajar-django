from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, HttpResponseRedirect
from django.urls import reverse
from django.template.loader import render_to_string

# Monthly dictionary
monthly_challenges_dict = {
    "january": "Eat no meet for entire month!",
    "february": "Walk for at least 20 minutes everyday!",
    'march': "Learn django for at least 1 hour everyday!",
    'april': "April is the month of the year",
    'may': "May is the month of the year",
    'june': "June is the month of the year",
    'july': "July is the month of the year",
    'august': "August is the month of the year",
    'september': "September is the month of the year",
    'october': "October is the month of the year",
    'november': "November is the month of the year",
    'december': "December is the month of the year",
}

# Create your views here.

def index(request):
    list_items = ""
    months = list(monthly_challenges_dict.keys())
    
    return render(request, "challenges/index.html", {
        'months': months,
    })

def january(request):
    return HttpResponse("This works!")

def february(request):
    return HttpResponse("walk for at least 20 minutes everyday")

def monthly_challenges_number(request, month):
    months = list(monthly_challenges_dict.keys())
    
    if month > len(months):
        return HttpResponseNotFound("This month does not exist")
    
    recirect_month = months[month - 1]
    redirect_path = reverse("monthly-challenges", args=[recirect_month]) # /challenge/january
    return HttpResponseRedirect(redirect_path)

def monthly_challenges(request, month):
    # try:
        challenge_text = monthly_challenges_dict[month]
        return render(request, "challenges/challenge.html", {
            'text': challenge_text,
            'month_name': month,
        })
    # except :
        # return HttpResponseNotFound("<h1>This month does not exist</h1>")
    
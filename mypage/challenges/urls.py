from unicodedata import name
from django.urls import path
from . import views

urlpatterns = [
    # path("january", views.january),
    # path("february", views.february),
    
    # Dynamic segments
    # path("<month>", views.monthly_challenges),
    
    path("", views.index), # /challenges/
    
    # Tell django to allow integer only values for month
    path("<int:month>", views.monthly_challenges_number),
    
    # Tell django to allow string only values for month
    path("<str:month>", views.monthly_challenges, name="monthly-challenges"),
    
]

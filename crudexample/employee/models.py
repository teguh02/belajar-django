from django.db import models

# Create your models here.
class Employee(models.Model):
    # Kolom kolom dibawah ini akan menjadi kolom di database
    # dan selain itu akan menjadi kolom di form secara otomatis
    
    # Buat kolom eid
    eid = models.CharField(max_length=20)
    
    # Buat kolom ename
    ename = models.CharField(max_length=100)
    
    # Buat kolom eemail
    eemail = models.EmailField(max_length=100)
    
    # Buat kolom econtact
    econtact = models.CharField(max_length=20)
    
    # Informasi meta tentang tabel
    class Meta:
        
        # Nama tabel yang akan dibuat
        db_table = 'employee'
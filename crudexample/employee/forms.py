# Import form libarary
from dataclasses import field
from django import forms

# Import Employee model
from .models import Employee

class EmployeeForm(forms.ModelForm):
    class Meta:
        model = Employee
        fields = '__all__'
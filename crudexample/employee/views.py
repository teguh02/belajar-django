from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404
import employee  
from employee.forms import EmployeeForm  
from employee.models import Employee  

# Create your views here.

# Store data if method is POST
# and show form if method is GET
def emp(request):
    # Jika metode POST
    if request.method == "POST" :
        # Tangkap semua request
        form = EmployeeForm(request.POST)
        
        # dan jika form valid
        if form.is_valid():
            try:
                # Simpan data
                form.save()
                
                # Redirect ke show
                return redirect('/show')
            except:
                pass
    else: 
            # Jika metode GET maka buat form kosong
            # berdasarkan model Employee
            form = EmployeeForm()
    return render(request, 'index.html', {'form':form})
        
# Show all employees data    
def show(request):
    employees = Employee.objects.all()
    return render(request, 'show.html', {'employees':employees})

# Edit an employee data by id
def edit(request, id):
    # employee = Employee.objects.get(id=id)
    employee = get_object_or_404(Employee, id=id)
    
    return render(request, 'edit.html', {'employee':employee})

def update(request, id):
    employee = Employee.objects.get(id=id)
    form = EmployeeForm(request.POST, instance=employee)
    
    if form.is_valid():
        form.save()
        return redirect('/show')
    
    return render(request, 'edit.html', {'employee': employee})

# Destroy data by id
def destroy(request, id):
    employee = Employee.objects.get(id=id)
    employee.delete()
    return redirect('/show')

from django.http import HttpResponse

# import library untuk merender html
from django.shortcuts import render

# Navigasi yang akan ditampilkan pada halaman website
# Menggunakan tipe data List
navigasi = [
    ['/', 'Beranda'],
    ['/tentang', 'Tentang'],
    ['/blog', 'Blog'],
    ['/contoh_template_variable', 'Varibel template'],
]

def tentang(request):
    variabel = {
        'nav': navigasi
    }
    return render(request, 'tentang.html', variabel)

def blog(request):
    variabel = {
        'nav': navigasi
    }
    return render(request, 'blog/index.html', variabel)

def index(request):
    variabel = {
        'nav': navigasi
    }
    return render(request, 'index.html', variabel)

def template_variable(request):
    # Deklarasi variabel dalam bentuk tipe data dictionary
    variabel={
        'judul': "Teguh Rijanandi",
        'body': 'Perkenalkan nama saya Teguh Rijanandi, saya seorang mahasiswa semeseter 5',
        'nav': navigasi
    }
    
    return render(request, 'template.html', variabel)

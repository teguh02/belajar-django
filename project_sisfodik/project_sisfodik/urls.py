"""project_sisfodik URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

# Import library http response untuk menampilkan
# respons http
from django.http import HttpResponse

# Method untuk menampilkan halaman tentang
# def tentang(request) :
#     return HttpResponse("Halaman tentang")

# Method untuk menampilkan halaman index
# def index(request):
#     return HttpResponse("Hello world !!")

# atau bisa import file views.py
# untuk meregister semua method tentang dan index

# from .views import index, tentang
# atau bisa juga
from .views import *

from . import views

# Untuk memuat static files seperti gambar, css, js dsb
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    
    # Ketika mengakses url tentang
    # akan dialihkan ke method tentang
    path('tentang/', tentang),
    
    # Ketika mengakses url index
    # akan dialihkan ke method index
    path('index', index),
    
    path('contoh_template_variable', template_variable),
    
    path('', views.index, name="index"),
    
    path('blog', blog),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
from django.db import models

# Create your models here.
from django.db import models

# Create your models here

# Membuat tabel ToDoList
class ToDoList(models.Model):
    
    # dengan struktur tabel berikut
    name = models.CharField(max_length=200)
    
    def __str__(self):
        return self.name
    
# Membuat tabel Item 
class Item(models.Model):
    
    # dengan struktur database seperti dibawah
    ToDoList = models.ForeignKey(ToDoList, on_delete=models.CASCADE)
    text = models.CharField(max_length=200)
    complete = models.BooleanField()
    
    def __str__(self):
        return self.text